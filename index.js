var express = require("express"),
app = express();
hbs = require('express-handlebars');
 
var posts = [{
                "subject": "Post numero 1",
                "description": "Descripcion 1",
                "time": new Date()
            },
            {
                "subject": "Post numero 2",
                "description": "Descripcion 2",
                "time": new Date()
            },
            {
                "subject": "Post numero 3",
                "description": "Descripcion 4",
                "time": new Date()
            }];

app.set("views", "./views");
app.engine("handlebars", hbs());
app.set("view engine", "handlebars");

//debuelve un objeto post de una request
function getPostObj(req){
	var post ={"subject" : req.query.subject,
				"description": req.query.description,
				 "time": new Date()
				};
	return post;
}

//****************************************************************************************Mustra todos los posts
app.get("/posts", function(req, res) {
	 res.render('posts', { "title": "Posts", post: posts  } );
	 console.log("Todos los posts");
});
//****************************************************************************************Muestra el ultimo post
app.get("/posts/new", function(req, res) {
	var post = posts[posts.length-1];
	res.render('posts', { "title": "El ultimo post es", "post": {post}  } );
	 console.log("Ultimo post");
});
//****************************************************************************************Crea un nuevo post
app.post("/posts", function(req, res) {
	posts.push(getPostObj(req));
 	console.log("post añadido");
});
//****************************************************************************************Edita el ultimo post
app.get("/posts/edit", function(req, res) {
	posts[posts.length-1] = getPostObj(req);
	res.render('posts', { "title": "El post se ha editado correctamente"} );
});
//****************************************************************************************Edita el post con esa ID
app.put("/posts/:id", function(req, res) {
	console.log("PUT PUT");
	console.log(req.params.id);
	if(req.params.id < posts.length){
		console.log( getPostObj(req));
		posts[req.params.id] = getPostObj(req);
		console.log( getPostObj(req));
	 	res.render('posts', { "title": "El post se ha editado correctamente"} );
	}
	else{
		res.render('posts', { "title": "No existe ese postasdasdas"} );
	}

});
//****************************************************************************************Recupera un post con Id
app.get("/posts/:id", function(req, res) {
	if(req.params.id < posts.length){
		var post = posts[req.params.id];
	 	res.render('posts', { "title": "Se ha encontrado el post", "post": {post}  } );
	}
	else{
		res.render('posts', { "title": "No existe ese post"} );
	}
	 console.log("Search by ID");
});
//****************************************************************************************Borra un post
app.delete("/posts/:id", function(req, res) {
	if(req.params.id < posts.length){
		posts.splice(req.params.id,1);
	 	res.render('posts', { "title": "Post borrado correctamente"} );
	}
	else{
		res.render('posts', { "title": "No existe ese post"} );
	}
	 console.log("Borrar Post");
});

app.listen(8080);